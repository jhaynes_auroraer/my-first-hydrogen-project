$eolcom #

option lp = mosek;

Set
         t_all 'All technologies -- inc power storage, hydrogen storage and hydrogen generation' /Hydrogen, CCGT, Wind, Solar, Battery, pumped_Hydro, Electrolysis, SMR, h_stor/
         c 'Set of commodities' /Power, Hydrogen/
         po(c) 'Power commodity'/Power/
         c_t_map(c,t_all) 'Mapping of tech to commodoties' /
         Power . (Hydrogen, CCGT, Wind, Solar, Battery, pumped_Hydro)
		     Hydrogen . (Electrolysis, SMR, h_stor)
		     /
		     g(t_all) 'Gas using techs' /CCGT, SMR/
         t(t_all) 'Set of power producing techs' /Hydrogen, CCGT, Wind, Solar, Battery, pumped_Hydro/ 
         h(t_all) 'Set of hyrogen producing techs' /Electrolysis, SMR, h_stor/
         t_lf(t_all) 'Load Factor Techs' /Wind, Solar/
         t_sto(t_all) 'Storage units' /battery, pumped_hydro, h_stor/
         p_sto(t_sto) 'Power Storage Units' /battery, pumped_hydro/
         h_sto(t_sto) 'Hydrogen Storage Units' /h_stor/
         z 'time segments' /1*100/
;
	
Parameter
         dem(z,c) 'Exogenous demand in energy (MWh) equivalent '         
         vom(t_all)  '[$/MW] per MW cost of producing power from technology t_all' /Hydrogen 15, CCGT 15, Wind 3, Solar 2, Battery 1, pumped_Hydro 1,Electrolysis 10, SMR 20,h_stor 5/
         p_cap(t_all) 'Capacity MW for all generating techs t' /Hydrogen 1000, CCGT 2500, Wind 6500, Solar 5500, Battery 200, pumped_Hydro 400, Electrolysis 1500, SMR 1500, h_stor 1000/
         curt_cost(c) 'cost of unmet demnad' /Power 1500, Hydrogen 1400/
         eff(t_all) 'Efficiency of production techs'  /Hydrogen 0.8, CCGT 0.9, Wind 1, Solar 1, Battery 1, pumped_Hydro 1, Electrolysis 0.5 , SMR 0.8, h_stor 1/ 
         s_eff(t_sto) 'Round Trip Efficiency of storage unit s' /battery 0.9,pumped_hydro 0.75, h_stor 1/
         sto_vol(t_sto) 'The maximum amount of energy that can be held in storage unit s, litres' /battery 300, pumped_hydro 150, h_stor 100/
         lf(z,t_lf) 'Load factor for load factor tech t_lf in segment z'
         gas_p(z) 'Gas price in all time segments ' 
         price(z,c) 'The derived price for each commodity'
         gen_results(z,t_all) 'The generating output of each tech'
         charging(z,t_sto)
;

$call 'csv2gdx demand.csv  ID=demID Index=1 Values=2..lastCol UseHeader=Y Output=demand.gdx'
$gdxin demand.gdx
$load dem = demID
$gdxin

$call 'csv2gdx lf1.csv  ID=lfID Index=1 Values=2..lastCol UseHeader=Y Output=lf1.gdx'
$gdxin lf1.gdx
$load lf = lfID
$gdxin

$call 'csv2gdx gas_price.csv  ID=gas_price_ID Index=1 Values=2..lastCol UseHeader=Y Output=gas_p.gdx'
$gdxin gas_p.gdx
$load gas_p = gas_price_ID
$gdxin

##

Variable
         xgen(t_all,z) 'Tech (t) generation (MW) for segment z'
         hgen(h,z) 'Hydrogen generation in segment z'
         obj 'Total cost'
         d_sto(t_sto,z) 'Difference in storage level'
         q_stor(t_sto,z) 'Amount of energy in storage unit s at segment z'
         sto_im(t_sto,z) 'Amount of energy inputed to storage unit s'
         dem_curt(z,c) 'Curtailment of demand'    
         z_cost(z)
;



Positive Variable
         xgen, hgen, curt, q_stor, sto_im, dem_curt, z_cost
;

Equations
         hydrogen_clear(z) 'Hydrogen Clearing'
         power_clear(z) 'Power Clearing'
         gen_cap(t_all,z) 'Limit generation for each generation tech dispenser for all time periods t'
         force_renewable(t_lf,z) 'Force renewable resource to be always it'
         sto_continu(t_sto,z) 'Development of storage level from one segment to another'
         sto_vol_lim(t_sto,z) 'Ensure the amount of energy at any one time is within capacity'
         segment_cost(z) 'Total cost in a segment not attributed to gas'
         total_cost 'Total cost across the day'
         sto_wrap(t_sto,z) 'Last period leads into first for storage'
;

#MARKET CLEARING##
* Ensure power balance and hydrogen market clear for all periods
power_clear(z) ..  sum(t, xgen(t,z)) + dem_curt(z,'Power') =e= dem(z,'Power') + xgen('Electrolysis',z)/eff('Electrolysis') + sum(p_sto, sto_im(p_sto,z));
hydrogen_clear(z) .. sum(h, xgen(h,z)) + dem_curt(z, 'Hydrogen') =e= dem(z,'Hydrogen') + xgen('Hydrogen',z)/eff('Hydrogen') + sum(h_sto, sto_im(h_sto,z));
* Ensure generation of all techs is within capacity
gen_cap(t_all,z) ..  xgen(t_all,z)  =l= p_cap(t_all);
* Ensure power generation of lf techs accounts for lf
force_renewable(t_lf,z) .. xgen(t_lf,z) =l= p_cap(t_lf)*lf(z,t_lf);

##STORAGE##
*Amount stored in Hydrogen Storage Units and power storage at segment z
sto_continu(t_sto,z)$(ord(z) > 1) .. q_stor(t_sto,z) =e= q_stor(t_sto,z-1) - xgen(t_sto,z-1) + sto_im(t_sto,z-1)*s_eff(t_sto);
*Wrap around year for both power storage and hydrogen storage
sto_wrap(t_sto,z)$(ord(z) = 1) .. q_stor(t_sto,z) =e= q_stor(t_sto,z+(card(z)-1)) -  xgen(t_sto,z+(card(z)-1)) + sto_im(t_sto,z+(card(z)-1))*s_eff(t_sto);
*Total amount stored should alwas respect volume of storage
sto_vol_lim(t_sto,z) .. q_stor(t_sto,z) =l= sto_vol(t_sto);

##COSTS##
segment_cost(z) .. z_cost(z) =e= sum(t_all, vom(t_all)*xgen(t_all,z)) + sum(g,xgen(g,z)*(gas_p(z)/eff(g))) + sum(c, dem_curt(z,c)*curt_cost(c));
total_cost .. obj =e= sum(z, z_cost(z));

Model my_first_hydrogen_dispatch 'my first hydrogen model' /all/;

Solve my_first_hydrogen_dispatch using lp minimizing obj;
price(z,'Hydrogen') = hydrogen_clear.M(z);
price(z,'Power') = power_clear.M(z);
gen_results(z,t_all) = xgen.L(t_all,z);
charging(z,t_sto) = sto_im.L(t_sto,z) + eps;

execute_unload 'My_first_hydrogen.gdx';


execute 'GDXXRW My_first_hydrogen.gdx o=prices.xlsx EpsOut=0 par=price rdim=1 cdim=1';
execute 'GDXXRW My_first_hydrogen.gdx o=generation.xlsx EpsOut=0 par=gen_results rdim=1 cdim=1';
execute 'GDXXRW My_first_hydrogen.gdx o=charging.xlsx EpsOut=0 par=charging rdim=1 cdim=1';
























